package Models;


public class Vision {

    private String name         ;
    private String description  ;
    private String timeLeft     ;
    private String createdAt    ;


    public Vision(String name, String description, String timeLeft, String createdAt) {
        this.name           = name;
        this.description    = description;
        this.timeLeft       = timeLeft;
        this.createdAt      = createdAt;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
