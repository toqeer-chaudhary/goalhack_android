package Adapters;

import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.goalhack.goalhack_android.R;
import com.goalhack.goalhack_android.VisionDetails;

import Models.Vision;

import java.util.List;
import java.util.ListIterator;

public class VisionAdapter extends RecyclerView.Adapter<VisionAdapter.ViewHodler> {

    private Context context ;
    private List<Vision> visionItems ;

    public VisionAdapter(Context context, List visionItems) {
        this.context = context;
        this.visionItems = visionItems;
    }

    @Override
    public VisionAdapter.ViewHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vision_main, parent, false);
        return new ViewHodler(view);
    }

    @Override
    public void onBindViewHolder(VisionAdapter.ViewHodler holder, int position) {

        Vision vision = visionItems.get(position);

        holder.name.setText(vision.getName());
        holder.description.setText(vision.getDescription());

    }

    @Override
    public int getItemCount() {
        return visionItems.size();
    }

    public class ViewHodler extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView name         ;
        private TextView description  ;
        private TextView createdAt    ;
        private TextView timeLeft     ;

        public ViewHodler(View visionView) {
            super(visionView);

            visionView.setOnClickListener(this);

            name        = (TextView) visionView.findViewById(R.id.visionId);
            description = (TextView) visionView.findViewById(R.id.descriptionId);
            createdAt   = (TextView) visionView.findViewById(R.id.createAtId);
            timeLeft    = (TextView) visionView.findViewById(R.id.timeLeftId);

        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            Vision item = visionItems.get(position);

            Intent intent = new Intent(context, VisionDetails.class);
            intent.putExtra("visionName",item.getName());
            intent.putExtra("visionNDescription",item.getDescription());
            intent.putExtra("visionTimeLeft",item.getTimeLeft());
            intent.putExtra("visionCreatedAt",item.getCreatedAt());
            context.startActivity(intent);

        }
    }
}
