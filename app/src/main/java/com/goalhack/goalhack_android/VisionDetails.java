package com.goalhack.goalhack_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class VisionDetails extends AppCompatActivity {

    private TextView visionName         ;
    private TextView visionDescription  ;
    private TextView visionCreatedAt    ;
    private TextView visionTimeLeft     ;
    private Bundle visionData           ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vision_details);

        visionData          = getIntent().getExtras();

        visionName          = (TextView) findViewById(R.id.visionName);
        visionDescription   = (TextView) findViewById(R.id.visionDescription);
        visionCreatedAt     = (TextView) findViewById(R.id.visionCreatedAt);
        visionTimeLeft      = (TextView) findViewById(R.id.visionTimelLeft);

        visionName.setText(visionData.getString("visionName"));
        visionDescription.setText(visionData.getString("visionNDescription"));
        visionCreatedAt.setText(visionData.getString("visionCreatedAt"));
        visionTimeLeft.setText(visionData.getString("visionTimeLeft"));

    }
}
